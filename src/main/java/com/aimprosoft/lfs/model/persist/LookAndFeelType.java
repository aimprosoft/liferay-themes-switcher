package com.aimprosoft.lfs.model.persist;

/**
 * The representation of allowed types of the {@link LookAndFeel}
 *
 * @author AimProSoft
 */
public enum LookAndFeelType {
    THEME, COLOR_SCHEME
}
