package com.aimprosoft.lfs.utils;

import com.aimprosoft.lfs.model.view.ResourcePermissions;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * the {@link ResourcePermissions} parametrised type for {@link ObjectMapper}
 *
 * @author AimProSoft
 */
public class ResourcePermissionTypeReference extends org.codehaus.jackson.type.TypeReference<ResourcePermissions> {
}
